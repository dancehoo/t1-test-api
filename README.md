# t1-test-api



## Assignment

![Link Name](images/API_Quick_Test.png)

## Require Environment

- Java version: 17

## Test and Deploy

Use the Gradle to build, run, report.

- clean build
```
./gradlew clean build
```
- jacoco Report (report directory: build/jacocoHtml)
```
./gradlew clean build jacocoTestReport
```
- run 
```
./gradlew bootRun
```

## Swagger
```
http://<host>:<port>/swagger-ui/index.html
```
***