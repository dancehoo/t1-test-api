package com.quick.test.t1.client

import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.web.bind.annotation.RestController

@ExtendWith(SpringExtension::class)
@SpringBootTest(
    classes = [RandomUserClientTest.TestConfiguration::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    value = [
        "logging.level.com.quick.test.t1=DEBUG",
        "spring.cloud.openfeign.client.config.default.loggerLevel=full"
    ]
)
@Tag("integration")
class RandomUserClientTest {

    @Configuration
    @EnableAutoConfiguration
    @RestController
    @EnableFeignClients(clients = [RandomUserClient::class])
    class TestConfiguration

    @Autowired
    lateinit var client: RandomUserClient

    @Nested
    inner class UserSeedTest {

        private val version = "1.4"
        private val seed = "Test"

        @Test
        fun shouldResponseSuccess() {
            val actual = client.seed(
                version = version,
                seed = seed
            )
            assertEquals(expected = version, actual = actual.info.version)
            assertEquals(expected = seed, actual = actual.info.seed)
            assertNotNull(actual.results[0].email)
            assertNotNull(actual.results[0].gender)
            assertNotNull(actual.results[0].name.first)
            assertNotNull(actual.results[0].name.last)
        }
    }
}