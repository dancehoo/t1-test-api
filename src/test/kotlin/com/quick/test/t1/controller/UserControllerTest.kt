package com.quick.test.t1.controller

import com.quick.test.security.model.HeaderInfo
import com.quick.test.t1.model.response.UserSeedResponse
import com.quick.test.t1.service.UserService
import org.hamcrest.MatcherAssert
import org.hamcrest.core.Is
import org.hamcrest.core.IsInstanceOf
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.eq
import org.mockito.kotlin.whenever

@ExtendWith(MockitoExtension::class)
@Tag("fast")
internal class UserControllerTest {
    @InjectMocks
    lateinit var subject: UserController

    @Mock
    lateinit var userService: UserService

    @Nested
    inner class SeedTest {

        private val seed = "Test"
        private val version = "1.4"
        private val headerInfo = HeaderInfo(version = version)
        private val users = listOf(
            UserSeedResponse(
                gender = "gender",
                firstName = "firstName",
                lastName = "lastName",
                email = "email@test.com"
            )
        )

        @Test
        fun shouldReturnSuccess() {
            whenever(userService.seed(eq(seed), eq(version))).thenReturn(users)
            val actual = subject.seed(
                headerInfo = headerInfo,
                seed = seed
            )
            MatcherAssert.assertThat(actual.code, Is.`is`("000"))
            MatcherAssert.assertThat(actual.data?.get(0), IsInstanceOf(UserSeedResponse::class.java))
        }

        @Test
        fun shouldReturnSuccessWhenVersionNull() {
            whenever(userService.seed(eq(seed), eq(null))).thenReturn(users)
            val actual = subject.seed(
                headerInfo = headerInfo.copy(version = null),
                seed = seed
            )
            MatcherAssert.assertThat(actual.code, Is.`is`("000"))
            MatcherAssert.assertThat(actual.data?.get(0), IsInstanceOf(UserSeedResponse::class.java))
        }
    }
}