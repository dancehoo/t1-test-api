package com.quick.test.t1.service

import com.google.gson.Gson
import com.quick.test.t1.client.RandomUserClient
import com.quick.test.t1.config.AppConfig.Companion.CURRENT_RANDOM_API_VERSION
import com.quick.test.t1.model.response.RandomUserClientResponse
import com.quick.test.t1.model.response.UserSeedResponse
import kotlin.test.assertEquals
import org.hamcrest.MatcherAssert
import org.hamcrest.core.IsInstanceOf
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.eq
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import org.springframework.util.ResourceUtils

@ExtendWith(MockitoExtension::class)
@Tag("fast")
internal class UserServiceImplTest {

    @InjectMocks
    lateinit var subject: UserServiceImpl

    @Mock
    lateinit var randomUserClient: RandomUserClient

    @Nested
    inner class SeedTest {
        private val seed = "Test"
        private val version = "1.4"
        private val inc = "gender,name,email"
        private val email = "alvin.sullivan@example.com"
        private val gender = "male"
        private val first = "Alvin"
        private val last = "Sullivan"
        private val seedShortResponse = ResourceUtils.getFile("classpath:user/random/client/seed_short_response_success.json")
        private val seedFullResponse = ResourceUtils.getFile("classpath:user/random/client/seed_full_response_success.json")

        @Test
        fun shouldSuccessSeedOnShortResponse() {
            whenever(randomUserClient.seed(eq(version), eq(seed), eq(inc))).thenReturn(
                Gson().fromJson(seedShortResponse.readText(), RandomUserClientResponse::class.java)
            )
            val actual = subject.seed(
                seed = seed,
                version = version
            )
            MatcherAssert.assertThat(actual[0], IsInstanceOf(UserSeedResponse::class.java))
            assertEquals(actual = actual.size, expected = 1)
            assertEquals(actual = actual[0].gender, expected = gender)
            assertEquals(actual = actual[0].firstName, expected = first)
            assertEquals(actual = actual[0].lastName, expected = last)
            assertEquals(actual = actual[0].email, expected = email)
            verify(randomUserClient, times(1)).seed(eq(version), eq(seed), eq(inc))
        }

        @Test
        fun shouldSuccessSeedOnFullResponse() {
            whenever(randomUserClient.seed(eq(version), eq(seed), eq(inc))).thenReturn(
                Gson().fromJson(seedFullResponse.readText(), RandomUserClientResponse::class.java)
            )
            val actual = subject.seed(
                seed = seed,
                version = version
            )
            MatcherAssert.assertThat(actual[0], IsInstanceOf(UserSeedResponse::class.java))
            assertEquals(actual = actual.size, expected = 1)
            assertEquals(actual = actual[0].gender, expected = gender)
            assertEquals(actual = actual[0].firstName, expected = first)
            assertEquals(actual = actual[0].lastName, expected = last)
            assertEquals(actual = actual[0].email, expected = email)
            verify(randomUserClient, times(1)).seed(eq(version), eq(seed), eq(inc))
        }
    }

    @Nested
    inner class VersionTest {
        @Test
        fun shouldReturnCurrentVersionWhenVersionNull() {
            val actual = subject.version(null)
            assertEquals(expected = CURRENT_RANDOM_API_VERSION, actual = actual)
        }

        @Test
        fun shouldReturnVersion() {
            val actual = subject.version("10")
            assertEquals(expected = "10", actual = actual)
        }
    }
}