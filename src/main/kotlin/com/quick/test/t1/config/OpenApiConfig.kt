package com.quick.test.t1.config

import com.quick.test.security.model.HeaderInfo
import com.quick.test.t1.config.AppConfig.Companion.CURRENT_RANDOM_API_VERSION
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.PathItem
import io.swagger.v3.oas.models.parameters.HeaderParameter
import io.swagger.v3.oas.models.servers.Server
import org.springdoc.core.customizers.GlobalOpenApiCustomizer
import org.springdoc.core.models.GroupedOpenApi
import org.springdoc.core.utils.SpringDocUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenApiConfig {

    @Bean
    fun publicApi(): GroupedOpenApi? {
        SpringDocUtils.getConfig().addRequestWrapperToIgnore(HeaderInfo::class.java)
        return GroupedOpenApi.builder()
            .group("T1")
            .packagesToScan("com.quick.test")
            .build()
    }

    @Bean
    fun globalHeaderOpenApiCustomizer(@Value("\${spring.application.name}") name: String): GlobalOpenApiCustomizer? {
        return GlobalOpenApiCustomizer { openApi: OpenAPI ->
            openApi.info.title("API Quick Test").description("Template Spring3").version("1.0")
            openApi.servers(listOf(Server().url("/")))
            openApi.paths.values.stream().flatMap { pathItem: PathItem -> pathItem.readOperations().stream() }
                .forEach { operation: Operation ->
                    operation.addParametersItem(HeaderParameter().name("X-Api-Version").required(false).example(CURRENT_RANDOM_API_VERSION))
                }
        }
    }
}