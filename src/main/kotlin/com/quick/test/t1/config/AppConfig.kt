package com.quick.test.t1.config

import com.quick.test.security.anotation.EnableSecurity
import com.quick.test.t1.client.RandomUserClient
import org.springframework.cloud.openfeign.EnableFeignClients

@EnableSecurity
@EnableFeignClients(
    clients = [
        RandomUserClient::class
    ]
)
class AppConfig {
    companion object {
        const val CURRENT_RANDOM_API_VERSION = "1.4"
    }
}