package com.quick.test.t1.config

import com.quick.test.t1.exception.UserBadRequestException
import feign.FeignException.errorStatus
import feign.Response
import feign.codec.ErrorDecoder

class FeignUserErrorDecoder : ErrorDecoder {
    override fun decode(methodKey: String?, response: Response?): Exception {
        val responseStatus = response?.status() ?: 0
        if (methodKey != null && methodKey.contains("RandomUserClient#seed", ignoreCase = true)) {
            when (responseStatus) {
                404 -> {
                    return UserBadRequestException(
                        statusCode = responseStatus,
                        message = "version not support"
                    )
                }
            }
        }
        return errorStatus(methodKey, response)
    }
}