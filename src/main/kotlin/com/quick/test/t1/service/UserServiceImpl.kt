package com.quick.test.t1.service

import com.quick.test.t1.client.RandomUserClient
import com.quick.test.t1.config.AppConfig.Companion.CURRENT_RANDOM_API_VERSION
import com.quick.test.t1.model.response.UserSeedResponse
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(
    val randomUserClient: RandomUserClient
) : UserService {

    override fun seed(seed: String, version: String?): List<UserSeedResponse> {
        return randomUserClient.seed(
            version = version(version),
            seed = seed
        ).results.map {
            UserSeedResponse(
                gender = it.gender,
                firstName = it.name.first,
                lastName = it.name.last,
                email = it.email
            )
        }
    }
    internal fun version(version: String?) = version ?: CURRENT_RANDOM_API_VERSION
}