package com.quick.test.t1.service

import com.quick.test.t1.model.response.UserSeedResponse

interface UserService {
    fun seed(seed: String, version: String?): List<UserSeedResponse>
}