package com.quick.test.t1.exception

import org.springframework.http.HttpStatus

data class UserBadRequestException(
    override val message: String? = null,
    val statusCode: Int? = null
) : RuntimeException(), ApiExceptionMessageSource {
    override val errorCode = "TEST01"
    override val httpStatus = HttpStatus.BAD_REQUEST
    override fun getCodes(): Array<String> = arrayOf("user.client.bad_request")
}