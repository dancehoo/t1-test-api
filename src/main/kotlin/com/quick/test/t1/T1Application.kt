package com.quick.test.t1

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(
    scanBasePackages = [
        "com.quick.test.t1"
    ]
)
class T1Application

fun main(args: Array<String>) {
    runApplication<T1Application>(*args)
}
