package com.quick.test.t1.model.response

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder("gender", "firstName", "lastName", "email")
data class UserSeedResponse(
    val gender: String,
    val firstName: String,
    val lastName: String,
    val email: String
)