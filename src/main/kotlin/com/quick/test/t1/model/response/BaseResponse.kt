package com.quick.test.t1.model.response

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("code", "message", "data", "cause")
data class BaseResponse<T>(
    var code: String,
    var message: String,
    var data: T? = null,
    var cause: String? = null
) {

    class Builder<T>() {
        fun success(data: T?): BaseResponse<T> {
            return BaseResponse("000", "SUCCESS", data, null)
        }

        fun error(code: String, message: String, data: T? = null, cause: String? = null): BaseResponse<T> {
            return BaseResponse(code, message, data, cause)
        }
    }
}