package com.quick.test.t1.model

data class Result(
    val gender: String,
    val name: Name,
    val email: String
)