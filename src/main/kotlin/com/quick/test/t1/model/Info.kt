package com.quick.test.t1.model

data class Info(
    val seed: String,
    val results: Int,
    val page: Int,
    val version: String
)