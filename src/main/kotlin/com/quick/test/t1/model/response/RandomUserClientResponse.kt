package com.quick.test.t1.model.response

import com.fasterxml.jackson.annotation.JsonInclude
import com.quick.test.t1.model.Info
import com.quick.test.t1.model.Result

@JsonInclude(JsonInclude.Include.NON_EMPTY)
data class RandomUserClientResponse(
    val results: List<Result>,
    val info: Info
)