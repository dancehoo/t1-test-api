package com.quick.test.t1.model

data class Name(
    val first: String,
    val last: String
)