package com.quick.test.t1

import com.quick.test.t1.config.LoggerDelegate
import com.quick.test.t1.exception.ApiExceptionMessageSource
import com.quick.test.t1.exception.UserBadRequestException
import com.quick.test.t1.model.response.BaseResponse
import jakarta.validation.ConstraintViolationException
import java.util.Locale
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatusCode
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {

    val log by LoggerDelegate()

    @ExceptionHandler(UserBadRequestException::class)
    fun userBadRequestExceptionException(
        ex: UserBadRequestException,
        locale: Locale
    ): ResponseEntity<BaseResponse<String>> {
        log.error("[UserBadRequestException] statusCode: {}, message: {}", ex.statusCode, ex.message)
        val response = BaseResponse.Builder<String>().error(ex.errorCode, messageSource?.getMessage(ex, locale).orEmpty())
        return ResponseEntity(response, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(ConstraintViolationException::class)
    fun constraintViolationException(
        ex: ConstraintViolationException,
        locale: Locale
    ): ResponseEntity<BaseResponse<String>> {
        log.error("[ConstraintViolationException]")
        val response = BaseResponse.Builder<String>().error("400", ex.message.orEmpty())
        return ResponseEntity(response, HttpStatus.BAD_REQUEST)
    }

    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatusCode,
        request: WebRequest
    ): ResponseEntity<Any>? {
        val errors = mutableListOf<String>()
        ex.bindingResult.fieldErrors.forEach { errors.add("${it.field}: ${it.defaultMessage}") }
        ex.bindingResult.globalErrors.forEach { errors.add("${it.objectName}: ${it.defaultMessage}") }
        log.error("[MethodArgumentNotValidException] errors: {}", errors)
        val response = BaseResponse.Builder<List<String>>()
            .error(
                code = "ARGS_ERROR",
                message = "Validation failed",
                data = errors
            )
        return ResponseEntity(response, status)
    }

    @ExceptionHandler(Exception::class)
    fun defaultException(ex: Exception, req: WebRequest, locale: Locale): ResponseEntity<BaseResponse<String>> {
        val errorCode = when (ex) {
            is ApiExceptionMessageSource -> ex.errorCode
            else -> "xxxx"
        }
        val message = when (ex) {
            is ApiExceptionMessageSource -> messageSource?.getMessage(ex, locale)
            else -> messageSource?.getMessage("default_error", null, locale)
        }
        val httpStatus = when (ex) {
            is ApiExceptionMessageSource -> ex.httpStatus
            else -> HttpStatus.BAD_REQUEST
        }

        log(httpStatus, ex)

        val response = BaseResponse.Builder<String>().error(
            code = errorCode,
            message = message.orEmpty(),
            data = null,
            cause = ex.message
        )
        return ResponseEntity(response, httpStatus)
    }

    private fun log(httpStatus: HttpStatus, ex: Exception) {
        if (httpStatus.isError) {
            log.error("error exception", ex)
        } else {
            log.debug("normal error", ex)
        }
    }
}