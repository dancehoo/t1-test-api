package com.quick.test.t1.controller

import com.quick.test.security.model.HeaderInfo
import com.quick.test.t1.config.AppConfig.Companion.CURRENT_RANDOM_API_VERSION
import com.quick.test.t1.model.response.BaseResponse
import com.quick.test.t1.model.response.UserSeedResponse
import com.quick.test.t1.service.UserService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import jakarta.validation.Valid
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Size
import org.springframework.http.MediaType
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/users")
@Validated
class UserController(
    private val userService: UserService
) {

    @GetMapping(
        value = ["/{seed}"],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    @Operation(
        summary = "API Quick Test",
        description = "<ul>" +
            "<li>Response random user from Random User API with input seed.</li>" +
            "<li>Current Api version is <b>$CURRENT_RANDOM_API_VERSION</b></li>" +
            "</ul>"
    )
    @ApiResponses(
        ApiResponse(responseCode = "200", description = "Success"),
        ApiResponse(responseCode = "404", description = "Incorrect version", content = arrayOf(Content()))
    )
    fun seed(
        @ModelAttribute
        @Valid
        headerInfo: HeaderInfo,
        @PathVariable
        @Size(min = 1, max = 256)
        @NotBlank
        seed: String
    ): BaseResponse<List<UserSeedResponse>> {
        return BaseResponse.Builder<List<UserSeedResponse>>().success(
            data = userService.seed(
                seed = seed,
                version = headerInfo.version
            )
        )
    }
}