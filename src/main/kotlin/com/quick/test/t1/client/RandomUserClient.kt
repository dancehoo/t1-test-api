package com.quick.test.t1.client

import com.quick.test.t1.config.FeignUserErrorDecoder
import com.quick.test.t1.model.response.RandomUserClientResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(value = "randomUserClient", configuration = [FeignUserErrorDecoder::class])
interface RandomUserClient {
    @GetMapping(
        value = ["/api/{version}/"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun seed(
        @PathVariable version: String,
        @RequestParam seed: String,
        @RequestParam inc: String = "gender,name,email"
    ): RandomUserClientResponse
}