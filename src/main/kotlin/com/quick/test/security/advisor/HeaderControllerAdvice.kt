package com.quick.test.security.advisor

import com.quick.test.security.model.HeaderInfo
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController

@ControllerAdvice(annotations = [RestController::class])
class HeaderControllerAdvice {
    @ModelAttribute(value = "headerInfo")
    fun addHeaderInfo(
        @RequestHeader(value = X_API_VERSION) version: String? = null
    ): HeaderInfo {
        return HeaderInfo(version)
    }

    companion object {
        const val X_API_VERSION = "X-Api-Version"
    }
}