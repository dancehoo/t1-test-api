package com.quick.test.security.model

import com.fasterxml.jackson.annotation.JsonInclude
import jakarta.validation.constraints.Size

@JsonInclude(JsonInclude.Include.NON_EMPTY)
data class HeaderInfo(
    @field:Size(max = 10)
    val version: String? = null
)