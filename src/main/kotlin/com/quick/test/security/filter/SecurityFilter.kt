package com.quick.test.security.filter

import com.quick.test.security.config.UrlPermitConfig
import org.springframework.context.annotation.Bean
import org.springframework.core.annotation.Order
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.SecurityFilterChain

class SecurityFilter(
    private val urlPermitConfig: UrlPermitConfig,
    private val authenticationManager: AuthenticationManager
) {

    @Bean
    @Throws(Exception::class)
    @Order(1)
    fun filterChain(http: HttpSecurity): SecurityFilterChain {
        return http.cors {
            it.disable()
        }.csrf {
            it.disable()
        }.sessionManagement {
            it.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        }.authorizeHttpRequests {
            it.requestMatchers(*urlPermitConfig.securityUrlPermit().urls()).permitAll()
            it.anyRequest().authenticated()
        }.authenticationManager(authenticationManager).build()
    }
}