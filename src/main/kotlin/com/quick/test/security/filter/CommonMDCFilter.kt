package com.quick.test.security.filter

import com.quick.test.security.advisor.HeaderControllerAdvice.Companion.X_API_VERSION
import com.quick.test.security.config.SecurityUrlPermit.Companion.NO_FILTER_URL
import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.core.annotation.Order
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.security.web.util.matcher.OrRequestMatcher
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter

@Order(1)
@Component
class CommonMDCFilter : OncePerRequestFilter() {
    private val MCD_X_VERSION = "X-Version"
    private val MCD_URI = "Uri"

    override fun shouldNotFilter(request: HttpServletRequest): Boolean {
        val matchers = OrRequestMatcher(NO_FILTER_URL.map { AntPathRequestMatcher(it) }.toList())
        return matchers.matches(request)
    }

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        if (isAsyncDispatch(request)) {
            filterChain.doFilter(request, response)
        } else {
            doFilterWrapped(request, response, filterChain)
        }
    }

    private fun doFilterWrapped(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        insertBeforeRequestMDC(request)
        try {
            filterChain.doFilter(request, response)
        } finally {
            fireLogResponse(response)
            cleanMDC()
        }
    }

    private fun fireLogResponse(response: HttpServletResponse) {
        val logger = LoggerFactory.getLogger(CommonMDCFilter::class.java)
        if (response.status >= 500) {
            logger.error("${response.status}")
        } else if (response.status >= 400) {
            logger.warn("${response.status}")
        } else {
            logger.info("${response.status}")
        }
    }

    private fun cleanMDC() {
        MDC.remove(MCD_X_VERSION)
        MDC.remove(MCD_URI)
    }

    private fun insertBeforeRequestMDC(request: HttpServletRequest) {
        val version = request.getHeader(X_API_VERSION).orEmpty()
        MDC.put(MCD_X_VERSION, version)
        MDC.put(MCD_URI, request.requestURI)
    }
}