package com.quick.test.security.anotation

import com.quick.test.security.filter.SecurityFilter
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity

@Retention(value = AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Configuration
@Import(SecurityFilter::class)
@EnableWebSecurity
@ComponentScan(basePackages = ["com.quick.test.security"])
annotation class EnableSecurity