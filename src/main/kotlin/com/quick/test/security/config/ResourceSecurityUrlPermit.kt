package com.quick.test.security.config

import com.quick.test.security.config.SecurityUrlPermit.Companion.NO_FILTER_URL

class ResourceSecurityUrlPermit : SecurityUrlPermit {
    override fun urls() = NO_FILTER_URL + arrayOf(
        "/v1/users/*"
    )
}