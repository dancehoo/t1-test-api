package com.quick.test.security.config

interface SecurityUrlPermit {
    fun urls(): Array<String>

    companion object {
        val NO_FILTER_URL = arrayOf(
            "/swagger-ui/**",
            "/v3/api-docs/**"
        )
    }
}