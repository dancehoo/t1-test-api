package com.quick.test.security.config

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class UrlPermitConfig {
    @Bean
    @ConditionalOnMissingBean
    fun securityUrlPermit(): SecurityUrlPermit {
        return ResourceSecurityUrlPermit()
    }
}